package wrappers;

import automate.Midtrans.SetUp_File;

public class WebPageOpen extends SetUp_File{

	public void PagURL() {
		 //maximize window
		  webdriver.manage().window().maximize();
		  
		  //opening page
		  webdriver.get("https://demo.midtrans.com/");
	}	
}
