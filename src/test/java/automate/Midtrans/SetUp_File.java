package automate.Midtrans;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


public class SetUp_File {

	public static WebDriver webdriver;
	protected ExtentReports report;
	protected ExtentTest test;
	
	
  @BeforeClass
  public void SetUp() {
	  // SetUp Extent Report
		  report = new ExtentReports(System.getProperty("D:\\Workspace folder\\Midtrans")+"\\test-output\\ExtentReportResults.html");
		  test = report.startTest("ExtentDemo");

	  // Setup Chrome Driver
	  System.setProperty("webdriver.chrome.driver", "D:\\softwares\\chromedriver_win32\\chromedriver.exe");
	  webdriver = new ChromeDriver();
	  webdriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  

  }

  @AfterClass 
  public void endTest() {
	  report.endTest(test);
	  report.flush();
	  webdriver.close();
  }
}
