package automate.Midtrans;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;

import wrappers.WebPageOpen;

public class AddItem extends WebPageOpen{
 
	protected WebElement iframe, buy_button, item_name,item_qty,item_amount,total_item_amount,cust_name,cust_email,cust_phone,cust_city,cust_address,postal_code,
	checkout_button,continue_button,credit_card,card_num,expiry,cvv,paynow,otp,submit, retry;	
	protected String item_nm,item_qt, item_amt, total_Amt, c_name,c_email, c_phone, c_city, c_address, p_code;
	
// Verifying Web Page	
  @Test(priority=1)
  public void WebPageOpen() {	  
	  
	  PagURL();
	  String actual_Title= webdriver.getTitle();
	  String expected_Title ="Sample Store";
	  
	  if(actual_Title== expected_Title) {
		  Assert.assertTrue(true, "Page Title is: " + actual_Title);
		  test.log(LogStatus.INFO, "Home Page open"); 
	  }else {
		  Assert.assertFalse(false, "Correct Page not open");
	  }  
  }
 
 // Click on Buy Item  
  @Test(priority=2)
  public void Click_BuyItem() {
	  buy_button = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[1]/div[2]/div/div/a"));
	  if(buy_button.isDisplayed()==true) {
		  //test.log(LogStatus.PASS, "Buy Button found");
		  Assert.assertTrue(true, "Buy Button found");
		  buy_button.click();		  
	  }else
	  {
		  test.log(LogStatus.FAIL, "Buy button not found");
	  }
  }
  
// Review Cart  
  @Test(priority =3)
  public void ReviewCart() {
	  item_name= webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[2]/table/tbody/tr[1]/td[1]"));
	  String item_nm= item_name.getText();
	  test.log(LogStatus.INFO, "Item name is: " + item_nm);

	  item_qty = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[2]/table/tbody/tr[1]/td[2]"));
	  item_qt= item_qty.getText();
	  test.log(LogStatus.INFO, "Item Quantity is: "+ item_qt);

	  item_amount = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[2]/table/tbody/tr[1]/td[3]/input"));
	  item_amt= item_amount.getAttribute("value");
	  test.log(LogStatus.INFO, "Item Amount is: "+ item_amt);
	  
	  total_item_amount = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[2]/table/tbody/tr[2]/td[3]"));		 
	  total_Amt = total_item_amount.getText();
	  test.log(LogStatus.INFO, "Total Item amount is: "+ total_Amt);
  }
  
  // Fill Customer Details
  @Test(priority=4)
  public void Customer_Details() {
	  test.log(LogStatus.INFO, "Customer Details are showing below "+"\n"+ "----------------" +"\n");
	  cust_name = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[4]/table/tbody/tr[1]/td[2]/input"));
	  cust_name.clear();
	  cust_name.sendKeys("Jayesh Bhandari");
	  c_name= cust_name.getAttribute("value");
	  test.log(LogStatus.INFO, "Customer Name is : " + c_name);
	 

	  cust_email= webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[4]/table/tbody/tr[2]/td[2]/input"));
	  cust_email.clear();
	  cust_email.sendKeys("testabc@gmail.com");
	  c_email = cust_email.getAttribute("value"); 
	  test.log(LogStatus.INFO, "Customer Email is : " + c_email);
	  
	  cust_phone= webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[4]/table/tbody/tr[3]/td[2]/input"));
	  cust_phone.clear();
	  cust_phone.sendKeys("9988776655");
	  c_phone =cust_phone.getAttribute("value");
	  test.log(LogStatus.INFO, "Customer Phone is : " + c_phone);	  
	  
	  cust_city  = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[4]/table/tbody/tr[4]/td[2]/input"));
	  cust_city.clear();
	  cust_city.sendKeys("Pune");
	  c_city= cust_city.getAttribute("value");
	  test.log(LogStatus.INFO, "Customer City is : " + c_city);
	  
	  cust_address = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[4]/table/tbody/tr[5]/td[2]/textarea"));
	  cust_address.clear();
	  cust_address.sendKeys("Kalyani Nagar");
	  c_address= cust_address.getAttribute("value");
	  test.log(LogStatus.INFO, "Customer Address is : " + c_address);
	  
	  postal_code = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[1]/div[4]/table/tbody/tr[6]/td[2]/input"));
	  postal_code.clear();
	  postal_code.sendKeys("11001");
	  p_code= postal_code.getAttribute("value");
	  test.log(LogStatus.INFO, "Postal code is: "+ p_code);
	  
	  test.log(LogStatus.INFO, "---------------");
  }
  
 // Showing Order Summary 
  @Test(priority=5)
  public void Order_Summary() {
	
	  checkout_button = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[2]/div[2]/div[1]"));
	  if(checkout_button.isEnabled()==true) {
		  System.out.println("Checkout button is Displayed and Enabled");
		  checkout_button.click();
		  test.log(LogStatus.PASS, "Clicked on Checkout button");
		  System.out.println("Clicked on Checkout button");
	  }else {
		  test.log(LogStatus.FAIL, "Checkout button is not displayed");
	      System.out.println("Checkout button is not displayed");
	  }
	 
	  iframe = webdriver.findElement(By.id("snap-midtrans"));
	  webdriver.switchTo().frame(iframe);
	  continue_button = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[1]/a"));
	  continue_button.click();
	  credit_card=  webdriver.findElement(By.xpath("//*[@id=\"payment-list\"]/div[1]/a"));
	  credit_card.click();
	  
	  }
  
  // Using Invalid Credit Card details
  @Test (priority=6)
  public void invalid_cc_Details() {
	  card_num= webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/form/div[2]/div[1]/input"));
	  card_num.sendKeys("4911111111111113");
	  
	  expiry = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/form/div[2]/div[2]/input"));
	  expiry.sendKeys("0221");
	  
	  cvv = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/form/div[2]/div[3]/input"));
	  cvv.sendKeys("123");
	  
	  paynow = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[1]/a"));
	  paynow.click();
	  
	  // final payout screen
	  iframe = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/iframe"));
	  webdriver.switchTo().frame(iframe);
	  
	  WebDriverWait wait= new WebDriverWait(webdriver, 90);
	  otp = webdriver.findElement(By.xpath("//*[@id=\"PaRes\"]"));
	  otp= wait.until(ExpectedConditions.visibilityOf(otp));
	  otp.sendKeys("112233");
	  submit = webdriver.findElement(By.xpath("//*[@id=\"acsForm\"]/div[6]/div/button[1]"));
	  submit.click();
	  WebElement failed_text= webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/div/div/div[2]"));
	  WebDriverWait wait_fail_text = new WebDriverWait(webdriver, 50);
	  failed_text = wait_fail_text.until(ExpectedConditions.visibilityOf(failed_text));
	  if(failed_text.isDisplayed()==true) {
	  // back to Payment options screen
	  webdriver.navigate().back();
	  webdriver.switchTo().defaultContent();
	  }
	  WebElement iframe5= webdriver.findElement(By.xpath("//*[@id=\"snap-midtrans\"]"));	  
	  webdriver.switchTo().frame(iframe5);
	  	  	  
	  WebDriverWait wait2 = new WebDriverWait(webdriver, 50);
	  WebElement cc1= webdriver.findElement(By.xpath("//*[@id=\"payment-list\"]/div[1]/a"));
	  cc1 = wait2.until(ExpectedConditions.elementToBeClickable(cc1));
	  if(cc1.isDisplayed()==true) {
	  test.log(LogStatus.PASS, "CC option is showing");
		  cc1.click();		  
	  }else {
		  test.log(LogStatus.FAIL, "CC option not showing");
	  }
  	}
   
 // Using vaild Credit Card details 
  @Test(priority=7)
  public void valid_cc_Details() {
	  //credit_card.click();
	  card_num= webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/form/div[2]/div[1]/input"));
	  card_num.sendKeys("4811111111111114");
	  
	  expiry = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/form/div[2]/div[2]/input"));
	  expiry.sendKeys("0221");
	  
	  cvv = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/form/div[2]/div[3]/input"));
	  cvv.sendKeys("123");
	  
	  paynow = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[1]/a"));
	  paynow.click();
	  payout();
  	}
  
  // final payout screen
  public void payout() {
	  
	  iframe = webdriver.findElement(By.xpath("//*[@id=\"application\"]/div[3]/div/div/div/iframe"));
	  webdriver.switchTo().frame(iframe);
	  
	  WebDriverWait wait= new WebDriverWait(webdriver, 90);
	  otp = webdriver.findElement(By.xpath("//*[@id=\"PaRes\"]"));
	  otp= wait.until(ExpectedConditions.visibilityOf(otp));
	  otp.sendKeys("112233");
	  submit = webdriver.findElement(By.xpath("//*[@id=\"acsForm\"]/div[6]/div/button[1]"));
	  submit.click();
	  WebElement succes_notification = webdriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div[1]/div[2]/div/div[2]"));
	  WebDriverWait success_wait = new WebDriverWait(webdriver, 50);
	  succes_notification= success_wait.until(ExpectedConditions.visibilityOf(succes_notification));
	  if(succes_notification.isDisplayed()==true) {
		  test.log(LogStatus.PASS, "Order purchase successfully");
	  }else {
		  test.log(LogStatus.FAIL, "Order Purchase failed");
	  }
  	}	 
}

	  
